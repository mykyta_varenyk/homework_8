package ua.dnipro.ntudp.varenyk_mykyta.library.web.controllers;

import org.junit.Test;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.bean.UserOrderBean;
import ua.dnipro.ntudp.varenyk_mykyta.library.model.User;
import ua.dnipro.ntudp.varenyk_mykyta.library.web.services.OrderService;

import javax.servlet.http.HttpSession;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class OrderControllerTest {
    private final OrderService orderService = mock(OrderService.class);

    private final HttpSession session = mock(HttpSession.class);

    private final OrderController sut = new OrderController(orderService,session);

    private final MockMvc mockMvc = MockMvcBuilders.standaloneSetup(sut).build();

    @Test
    public void shouldReturnUserOrders() throws Exception {
        UserOrderBean userOrderBean1 = UserOrderBean.builder()
                .setOrderId(1)
                .setUserId(1)
                .setAuthor("author")
                .setName("name")
                .setPublisher("publisher")
                .setApprovedTime(LocalDateTime.now())
                .setBookId(1)
                .setDaysCount(3)
                .setLibrarianId(2)
                .setReturned(true)
                .build();

        UserOrderBean userOrderBean2 = UserOrderBean.builder()
                .setOrderId(1)
                .setUserId(1)
                .setAuthor("author")
                .setName("name")
                .setPublisher("publisher")
                .setApprovedTime(LocalDateTime.now())
                .setBookId(1)
                .setDaysCount(3)
                .setLibrarianId(2)
                .setReturned(true)
                .build();

        int currentPage = 1;

        int recordsPerPage = 2;

        String languageCode = "en";

        when(session.getAttribute("language")).thenReturn(languageCode);

        User user = mock(User.class);

        when(user.getId()).thenReturn(1);

        List<UserOrderBean> orders = Arrays.asList(userOrderBean1, userOrderBean2);

        when(orderService.getUserOrderBeansWithPagination(currentPage, recordsPerPage, languageCode, 1))
                .thenReturn(orders);

        when(session.getAttribute("user")).thenReturn(user);

        mockMvc.perform(get("/controller/user/orders").sessionAttr("user",user))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(model().attribute("userOrderBeans", hasSize(2)))
                .andExpect(model().attribute("userOrderBeans", is(orders)));
    }
}