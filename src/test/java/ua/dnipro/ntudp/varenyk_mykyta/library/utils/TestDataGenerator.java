package ua.dnipro.ntudp.varenyk_mykyta.library.utils;

import ua.dnipro.ntudp.varenyk_mykyta.library.db.bean.UserOrderBean;
import ua.dnipro.ntudp.varenyk_mykyta.library.model.Book;
import ua.dnipro.ntudp.varenyk_mykyta.library.model.Role;
import ua.dnipro.ntudp.varenyk_mykyta.library.model.User;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class TestDataGenerator {
    private static final Random random = new Random();

    private final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    public static List<Book> generateBooks(int counter){
        return IntStream.range(0, counter)
                .mapToObj(TestDataGenerator::createBook)
                .collect(Collectors.toList());
    }

    private static Book createBook(int counter){
        List<String> author = new ArrayList<>();
        author.add("Fedor Dostoevsky");
        return Book.builder()
                .setId(random.nextInt())
                .setName("test")
                .setAuthor(author)
                .setDateOfIssue(LocalDate.parse("2020-12-03"))
                .setDate(LocalDate.now().toString())
                .setPublisher("First Publisher")
                .setDescription("description")
                .setCount(random.nextInt())
                .setPublisherUa("������ ��������")
                .setDescriptionUa("����")
                .setNameUa("��'�")
                .setReturned(random.nextBoolean())
                .build();
    }

    public static List<User> generateUsers(int counter){
        return IntStream.range(0,counter)
                .mapToObj(TestDataGenerator::createUser)
                .collect(Collectors.toList());
    }

    private static User createUser(int value){
        return User.builder()
                .setBlocked(true)
                .setEmail("email")
                .setFirstName("name")
                .setLastName("last name")
                .setFirstNameUa("��'�")
                .setLastNameUa("�������")
                .setHasOrders(random.nextBoolean())
                .setLogin("login")
                .setPassword("password")
                .setRoleId(random.nextInt(4))
                .setRole(Role.USER.getName())
                .build();
    }

    public static List<UserOrderBean> generateUserOrderBeans(int counter){
        return IntStream.range(0,counter)
                .mapToObj(TestDataGenerator::createUserOrderBean)
                .collect(Collectors.toList());
    }

    private static UserOrderBean createUserOrderBean(int count){
        return UserOrderBean.builder()
                .setOrderId(random.nextInt())
                .setBookId(random.nextInt())
                .setApprovedTime(LocalDateTime.now())
                .setAuthor("author")
                .setDaysCount(3)
                .setLibrarianId(2)
                .setName("name")
                .setPublisher("publisher")
                .setUserId(random.nextInt())
                .setInReadingHall(false)
                .setReturned(true)
                .build();
    }
}
