package ua.dnipro.ntudp.varenyk_mykyta.library.db.dao;

import org.junit.Before;
import org.junit.Test;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.DBManager;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.bean.UserOrderBean;
import ua.dnipro.ntudp.varenyk_mykyta.library.model.Order;

import javax.sql.DataSource;

import java.util.List;

import static org.junit.Assert.*;

public class OrderDaoTest {
    private DataSource dataSource;

    private OrderDao orderDao;

    @Before
    public void setUp(){
        dataSource = DBManager.getDataSourceForTests();
        orderDao = new OrderDao(dataSource);
    }

    @Test
    public void checkCorrectNumberOfOrders(){
        int id = 1;
        int numberOfOrders = orderDao.getOrdersAmountForUser(id);

        assertEquals(numberOfOrders,orderDao.getOrdersAmountForUser(id));
    }

    @Test
    public void shouldReturnOrdersWithSpecifiedLimitAndOffset(){
        int currentPage = 1;

        int recordsPerPage = 2;

        int userId = 1;

        String languageCode = "en";

        List<UserOrderBean> userOrderBeans = orderDao.getUserOrderBeansByUserId(userId,languageCode,currentPage,recordsPerPage);

        assertEquals(userOrderBeans.size(),orderDao.getUserOrderBeansByUserId(userId,languageCode,currentPage,recordsPerPage).size());
    }

    @Test
    public void shouldCreateAndApproveTakeHomeOrder(){
        Order order = new Order();

        order.setAccountID(1);

        order.setBookId(30);

        order.setDaysCount(3);

        assertTrue(orderDao.createTakeHomeOrder(order));

        List<UserOrderBean> userOrderBeans = orderDao.getAllUserOrderBeans("en");

        order.setId((int) userOrderBeans.get(userOrderBeans.size()-1).getOrderId());

        assertTrue(orderDao.approveOrder(7,order.getId()));
    }

    @Test
    public void shouldCreateAndDisapproveInReadingHallOrder(){
        Order order = new Order();

        order.setAccountID(1);

        order.setBookId(30);

        order.setHoursCount(3);

        assertTrue(orderDao.createInReadingHallOrder(order));

        List<UserOrderBean> userOrderBeans = orderDao.getAllUserOrderBeans("en");

        order.setId((int) userOrderBeans.get(userOrderBeans.size() -1).getOrderId());

        assertTrue(orderDao.disapproveOrder(7, order.getId()));

    }

    @Test
    public void shouldReturnAllOrders(){
        List<Order> orders = orderDao.getAllOrders();

        assertEquals(orders.size(),orderDao.getAllOrders().size());
    }


}