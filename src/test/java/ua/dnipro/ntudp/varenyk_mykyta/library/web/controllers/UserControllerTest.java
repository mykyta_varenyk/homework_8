package ua.dnipro.ntudp.varenyk_mykyta.library.web.controllers;

import org.junit.Test;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import ua.dnipro.ntudp.varenyk_mykyta.library.model.Role;
import ua.dnipro.ntudp.varenyk_mykyta.library.model.User;
import ua.dnipro.ntudp.varenyk_mykyta.library.web.services.UserService;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class UserControllerTest {
    private final UserService userService = mock(UserService.class);

    private final UserController sut = new UserController(userService);

    private final MockMvc mockMvc = MockMvcBuilders.standaloneSetup(sut).build();


    @Test
    public void shouldAdminUsersPage() throws Exception {
        User user1 = User.builder()
                .setBlocked(false)
                .setHasOrders(true)
                .setRole(Role.USER.getName())
                .setEmail("email")
                .setFirstName("first name")
                .setLastName("last name")
                .setFirstNameUa("��'�")
                .setLastNameUa("�������")
                .setLogin("login")
                .setPassword("password")
                .setRoleId(1)
                .build();

        User user2 = User.builder()
                .setBlocked(false)
                .setHasOrders(true)
                .setRole(Role.USER.getName())
                .setEmail("email")
                .setFirstName("first name")
                .setLastName("last name")
                .setFirstNameUa("��'�")
                .setLastNameUa("�������")
                .setLogin("login")
                .setPassword("password")
                .setRoleId(1)
                .build();

        int currentPage = 1;

        int recordsPerPage = 2;

        String languageCode = "en";

        List<User> users = Arrays.asList(user1,user2);

        when(userService.getUsersWithPagination(currentPage,recordsPerPage)).thenReturn(users);

        mockMvc.perform(get("/controller/admin/users"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(model().attribute("users", hasSize(2)))
                .andExpect(model().attribute("users",is(users)));
    }
}