package ua.dnipro.ntudp.varenyk_mykyta.library.web.controllers;

import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.dao.OrderDao;
import ua.dnipro.ntudp.varenyk_mykyta.library.model.Author;
import ua.dnipro.ntudp.varenyk_mykyta.library.model.Book;
import ua.dnipro.ntudp.varenyk_mykyta.library.util.Util;
import ua.dnipro.ntudp.varenyk_mykyta.library.web.services.BookService;

import javax.servlet.http.HttpSession;
import java.time.LocalDate;
import java.util.*;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class BookControllerTest {
    private final BookService bookService = mock(BookService.class);

    private final HttpSession session = mock(HttpSession.class);

    private final OrderDao orderDao = mock(OrderDao.class);

    private final BookController sut = new BookController(bookService, session, orderDao);

    private final MockMvc mockMvc = MockMvcBuilders.standaloneSetup(sut).build();

    @Test
    public void findBooksShouldReturnBooksDetails() throws Exception {
        Book book1 = Book.builder()
                .setName("Crime and Punishment")
                .setAuthor(Collections.singletonList("Fedor Dostoevsky"))
                .setCount(3)
                .setDate("1977-07-07")
                .setDateOfIssue(LocalDate.parse("1977-07-07"))
                .setDescription("Seventh publisher")
                .setPublisher("Eighth publisher")
                .build();

        String languageCode = "en";

        when(session.getAttribute("language")).thenReturn(languageCode);

        when(bookService.findBooks(languageCode, null, "yes", "Fedor Dostoevsky", "Crime and Punishment"))
                .thenReturn(Collections.singletonList(book1));

        mockMvc.perform(get("/controller/search")
                .param("name-id", "Crime and Punishment")
                .param("search-by-name", "yes")
                .param("author-id", "Fedor Dostoevsky")
                .accept(MediaType.TEXT_HTML))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(model().attribute("bookBeans", hasSize(1)))
                .andExpect(model().attribute("bookBeans", hasItem(hasProperty("name", is("Crime and Punishment")))));
    }

    @Test
    public void shouldReturnSortedBooks() throws Exception {
        Book book1 = Book.builder()
                .setId(33)
                .setName("The Lord of The Rings")
                .setAuthor(Collections.singletonList("John R.R. Tolkien"))
                .setCount(3)
                .setDate("2020-08-01")
                .setDateOfIssue(LocalDate.parse("2020-08-01"))
                .setDescription("Seventh publisher")
                .setPublisher("Eighth publisher")
                .build();

        Book book2 = Book.builder()
                .setId(2)
                .setName("The Hobbit")
                .setAuthor(Collections.singletonList("John R.R. Tolkien"))
                .setCount(7)
                .setDate("2004-09-18")
                .setDateOfIssue(LocalDate.parse("2004-09-18"))
                .setPublisher("Second publisher")
                .build();

        List<Book> books1 = Arrays.asList(book1, book2);

        when(session.getAttribute("booksList"))
                .thenReturn(books1);

        String sortBy = "name";

        List<Book> books2 = new ArrayList<>(books1);

        books2.sort(Comparator.comparing(Book::getName));

        doReturn(books2).when(bookService).sortBooks(books1, sortBy);

        mockMvc.perform(get("/controller/sort")
                .accept(MediaType.TEXT_HTML)
                .param("sort_by",sortBy))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(model().attribute("booksList", hasSize(2)))
                .andExpect(model().attribute("booksList", is(books2)));
    }

    @Test
    public void shouldReturnAdminsBookPage() throws Exception {
        String languageCode = "en";

        when(session.getAttribute("language")).thenReturn(languageCode);

        when(Util.getLanguageCodeFromSession(session)).thenReturn(languageCode);

        int currentPage = 1;

        int pageSize = 2;

        Book book1 = Book.builder()
                .setName("Crime and Punishment")
                .setAuthor(Collections.singletonList("Fedor Dostoevsky"))
                .setCount(3)
                .setDate("1977-07-07")
                .setDateOfIssue(LocalDate.parse("1977-07-07"))
                .setDescription("Seventh publisher")
                .setPublisher("Eighth publisher")
                .build();

        Book book2 = Book.builder()
                .setId(2)
                .setName("The Lord of The Rings")
                .setAuthor(Collections.singletonList("John R.R. Tolkien"))
                .setCount(7)
                .setDate("2004-09-18")
                .setDateOfIssue(LocalDate.parse("2004-09-18"))
                .setPublisher("Second publisher")
                .build();

        List<Book> books = Arrays.asList(book1, book2);

        when(bookService.getBooksWithPagination(currentPage, pageSize, languageCode))
                .thenReturn(books);

        when(bookService.getAllAuthors(languageCode)).thenReturn(Arrays.asList(new Author("Fedor Dostoevsky"), new Author("John R.R. Tolkien")));

        mockMvc.perform(get("/controller/admin/books"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(model().attribute("authors", hasSize(2)))
                .andExpect(model().attribute("books", hasSize(2)))
                .andExpect(model().attribute("books", is(books)));
    }
}