package ua.dnipro.ntudp.varenyk_mykyta.library.web.services.impl;

import org.junit.Test;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.bean.UserOrderBean;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.dao.OrderDao;
import ua.dnipro.ntudp.varenyk_mykyta.library.utils.TestDataGenerator;
import ua.dnipro.ntudp.varenyk_mykyta.library.web.services.OrderService;

import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class OrderServiceImplTest {
    private final OrderDao orderDao = mock(OrderDao.class);

    private final OrderService sut = new OrderServiceImpl(orderDao);

    @Test
    public void shouldReturnUserOrderBeansWithPagination(){
        int currentPage = 1;

        int recordsPerPage = 2;

        String languageCode = "en";

        int id = 1;

        List<UserOrderBean> beans = TestDataGenerator.generateUserOrderBeans(2);

        doReturn(beans).when(orderDao).getUserOrderBeansByUserId(id,languageCode,currentPage,recordsPerPage);

        List<UserOrderBean> beansList = sut.getUserOrderBeansWithPagination(currentPage,recordsPerPage,languageCode,id);

        assertEquals(beans,beansList);
    }
}