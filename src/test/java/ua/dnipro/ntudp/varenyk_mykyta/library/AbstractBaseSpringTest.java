package ua.dnipro.ntudp.varenyk_mykyta.library;

import com.mysql.cj.jdbc.MysqlConnectionPoolDataSource;
import com.mysql.cj.jdbc.MysqlDataSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.JdbcTransactionObjectSupport;
import org.springframework.jdbc.support.JdbcTransactionManager;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionManager;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import java.sql.SQLException;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@RunWith(SpringRunner.class)
@WebAppConfiguration
@ContextConfiguration
public abstract class AbstractBaseSpringTest {
    private static final Logger LOGGER = LogManager.getLogger();
    @Autowired
    protected MockMvc mockMvc;

    @Configuration
    @EnableWebMvc
    @ComponentScan("ua.dnipro.ntudp.varenyk_mykyta.library")
    protected static class TestContextConfiguration {
        @Bean
        public MockMvc mockMvc(WebApplicationContext webApplicationContext) {
            return MockMvcBuilders
                    .webAppContextSetup(webApplicationContext)
                    .apply(SecurityMockMvcConfigurers.springSecurity())
                    .alwaysDo(print())
                    .build();
        }

        @Bean
        public static DataSource getDataSource() {
            MysqlDataSource mysqlDataSource = null;
            try {
                mysqlDataSource = new MysqlConnectionPoolDataSource();
                mysqlDataSource.setURL("jdbc:mysql://localhost:3306/library");
                mysqlDataSource.setUser("root");
                mysqlDataSource.setPassword("15092023Mykyta");
                mysqlDataSource.setServerTimezone("UTC");
                mysqlDataSource.setAllowMultiQueries(true);
            } catch (SQLException throwables) {
                LOGGER.debug("SQLException in DBManager#getDataSourceForTests() -> {}", throwables);
            }
            return mysqlDataSource;
        }

        @Bean(name = "myTransactionManager")
        public static DataSourceTransactionManager transactionManager() {
            JdbcTransactionManager transactionManager = new JdbcTransactionManager();

            transactionManager.setDataSource(getDataSource());
            return transactionManager;
        }
    }
}
