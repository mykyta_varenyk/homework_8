package ua.dnipro.ntudp.varenyk_mykyta.library.web.controllers;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.transaction.annotation.Transactional;
import ua.dnipro.ntudp.varenyk_mykyta.library.AbstractBaseSpringTest;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.dao.BookDao;
import ua.dnipro.ntudp.varenyk_mykyta.library.model.Book;
import ua.dnipro.ntudp.varenyk_mykyta.library.utils.TestDataGenerator;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class BookControllerIT extends AbstractBaseSpringTest {
    @Autowired
    private BookDao bookDao;

    @Test
    @WithMockUser(username = "admin", roles = "ADMIN")
    @Transactional(transactionManager = "myTransactionManager")
    public void bookCreationWorksThroughAllLayers() throws Exception {
        Book book = TestDataGenerator.generateBooks(1).get(0);
        int authorId = 1;

        mockMvc.perform(post("/controller/admin/create-book").flashAttr("book", book)
                .sessionAttr("language", "en")
                .param("authorId", String.valueOf(authorId))
                .param("dateOfIssueInTheFuture", (String) null))
                .andExpect(status().is3xxRedirection());

        int numOfBooks = bookDao.getNumberOfBooks();

        int recordsPerPage = 2;

        int numOfPages = (int) Math.ceil(numOfBooks * 1.0 / recordsPerPage);

        int page = 0;

        for (int i = 0; i < numOfPages; i += recordsPerPage) {
            page += i;
        }

        page -=2;

        mockMvc.perform(get("/controller/admin/books")
                .sessionAttr("language", "en")
                .param("page",String.valueOf(page)))
                .andExpect(model().attribute("books", hasItem(allOf(
                        hasProperty("id", is(book.getId())),
                        hasProperty("name", is(book.getName())),
                        hasProperty("author", is(book.getAuthor())),
                        hasProperty("dateOfIssue", is(book.getDateOfIssue())),
                        hasProperty("date", is(book.getDate())),
                        hasProperty("publisher", is(book.getPublisher())),
                        hasProperty("description", is(book.getDescription())),
                        hasProperty("count", is(book.getCount())),
                        hasProperty("publisherUa", is(book.getPublisherUa())),
                        hasProperty("descriptionUa", is(book.getDescriptionUa())),
                        hasProperty("nameUa", is(book.getNameUa())),
                        hasProperty("returned", is(book.isReturned()))
                ))));
    }
}
