package ua.dnipro.ntudp.varenyk_mykyta.library.web.services.impl;

import org.junit.Test;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.dao.BookDao;
import ua.dnipro.ntudp.varenyk_mykyta.library.model.Book;
import ua.dnipro.ntudp.varenyk_mykyta.library.utils.TestDataGenerator;
import ua.dnipro.ntudp.varenyk_mykyta.library.web.services.BookService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class BookServiceImplTest {
    private final BookDao bookDao = mock(BookDao.class);

    private final BookService sut = new BookServiceImpl(bookDao);

    @Test
    public void shouldFindBooks() {
        Book book = TestDataGenerator.generateBooks(1).get(0);

        String languageCode = "en";

        doReturn(Collections.singletonList(book)).when(bookDao).findBookBeansByNameAndAuthor(languageCode, book.getAuthor().get(0), book.getName());

        List<Book> foundedBooks = sut.findBooks(languageCode, "yes", "yes", book.getAuthor().get(0), book.getName());

        verify(bookDao).findBookBeansByNameAndAuthor(languageCode, book.getAuthor().get(0), book.getName());
        assertThat(foundedBooks, hasSize(1));

        foundedBooks.forEach(
                book1 -> assertThat(foundedBooks, hasItem(allOf(
                        hasProperty("id", is(book.getId())),
                        hasProperty("name", is(book.getName())),
                        hasProperty("author", is(book.getAuthor())),
                        hasProperty("dateOfIssue", is(book.getDateOfIssue())),
                        hasProperty("date", is(book.getDate())),
                        hasProperty("publisher", is(book.getPublisher())),
                        hasProperty("description", is(book.getDescription())),
                        hasProperty("count", is(book.getCount())),
                        hasProperty("publisherUa", is(book.getPublisherUa())),
                        hasProperty("descriptionUa", is(book.getDescriptionUa())),
                        hasProperty("nameUa", is(book.getNameUa())),
                        hasProperty("returned", is(book.isReturned()))
                )))
        );
    }

    @Test
    public void shouldSortBooks() {
        List<Book> books1 = TestDataGenerator.generateBooks(3);

        String sortBy = "author";

        List<Book> books2 = new ArrayList<>(books1);

        books2.sort((b1, b2) -> {
            int result = b1.getAuthor().get(0).
                    compareTo(b2.getAuthor().get(0));
            if (result == 0) {
                return b1.getName().compareTo(b2.getName());
            }
            return result;
        });

        List<Book> sortedBooks = sut.sortBooks(books1, sortBy);

        assertEquals(books2, sortedBooks);
    }

    @Test
    public void shouldReturnBooksWithPagination() {
        int currentPage = 1;

        int recordsPerPage = 2;

        String languageCode = "en";

        List<Book> books = TestDataGenerator.generateBooks(2);

        doReturn(books).when(bookDao).getBooks(currentPage,recordsPerPage,languageCode);

        List<Book> bookList = sut.getBooksWithPagination(currentPage,recordsPerPage,languageCode);

        assertEquals(books,bookList);
    }
}