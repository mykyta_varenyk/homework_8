package ua.dnipro.ntudp.varenyk_mykyta.library.web.services.impl;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.security.crypto.password.PasswordEncoder;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.dao.UserDao;
import ua.dnipro.ntudp.varenyk_mykyta.library.model.User;
import ua.dnipro.ntudp.varenyk_mykyta.library.utils.TestDataGenerator;
import ua.dnipro.ntudp.varenyk_mykyta.library.web.services.UserService;

import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class UserServiceImplTest {
    private final UserDao userDao = mock(UserDao.class);

    private final PasswordEncoder passwordEncoder = mock(PasswordEncoder.class);

    private final UserService sut = new UserServiceImpl(userDao,passwordEncoder);

    @Test
    public void shouldReturnUsersWithPagination(){
        int currentPage = 1;

        int recordsPerPage = 2;

        List<User> userList = TestDataGenerator.generateUsers(2);

        doReturn(userList).when(userDao).getUsers(currentPage,recordsPerPage);

        List<User> users = userDao.getUsers(currentPage,recordsPerPage);

        assertEquals(users,userList);
    }
}