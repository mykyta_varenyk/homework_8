<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ taglib prefix="f" uri="http://library.varenyk_mykyta.ntudp.dnipro.ua/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="h" tagdir="/WEB-INF/tags" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : 'en'}" scope="session" />

<fmt:setBundle basename="text"/>


<html>
<head>
    <title>Admin/Edit-book</title>
</head>
<body>
<div class="container">
<fmt:message key="edit-book_jsp.editBook"/>
<div class="col s4">
    <div class="show-edit-form">
        <form action="${pageContext.request.contextPath}/controller/admin/update-book" method="post">
            <label for="author">Author/s:</label>
            <select class="browser-default" id="author" name="authorId">
                <c:forEach var="author" items="${requestScope.authors}">
                    <option value="${author.id}">${author.name}</option>
                </c:forEach>
            </select>
            <br>
            <label for="publisherUa"><fmt:message key="label.publisher"/> Українська: </label>
            <input class="browser-default" type="text" id="publisherUa" name="publisherUa"
                   required
                   maxlength="255" value="${requestScope.book.publisherUa}">
            <br>

            <label for="descriptionUa"><fmt:message key="label.description"/> Українська: </label>
            <input class="browser-default" type="text" id="descriptionUa" name="descriptionUa"
                   required
                   maxlength="255" value="${requestScope.book.descriptionUa}">
            <br>

            <label for="nameUa"><fmt:message key="label.name"/> Українська: </label>
            <input class="browser-default" type="text" id="nameUa" name="nameUa"
                   required
                   maxlength="255" value="${requestScope.book.nameUa}">
            <br>

            <label for="publisher"><fmt:message key="label.publisher"/> English: </label>
            <input class="browser-default" type="text" id="publisher" name="publisher"
                   required
                   maxlength="255" value="${requestScope.book.publisher}">
            <br>

            <label for="description"><fmt:message key="label.description"/> English: </label>
            <input class="browser-default" type="text" id="description" name="description"
                   required
                   maxlength="255" value="${requestScope.book.publisher}">
            <br>

            <label for="name"><fmt:message key="label.name"/> English: </label>
            <input class="browser-default" type="text" id="name" name="name"
                   required
                   maxlength="255" value="${requestScope.book.name}">
            <br>

            <label for="date_of_issue"><fmt:message key="label.dateOfIssue"/>: </label>
             <input class="browser-default" type="date" id="date_of_issue" name="date_of_issue"
                    min="0000-01-01" max="2030-09-30" value="${requestScope.book.dateOfIssue}">
             <br>

            <label for="count"><fmt:message key="label.count"/>: </label>
            <input id="count" type="number" min="0" max="1500" required
                   placeholder="0-1500" name="count" value="${requestScope.book.count}">
            <br>
            <input type="number" hidden name="bookId" value="${requestScope.id}"/>
            <input type="submit" class="btn" value="<fmt:message key="button.update"/>"/>
        </form>
    </div>
</div>
</div>
</div>
</div>
</div>

<h:change-language path="redirect:/FinalProject/controller/admin/edit-book"/>
<%@include file="/WEB-INF/jspf/footer.jspf"%>

</body>
</html>
