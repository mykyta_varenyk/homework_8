<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://library.varenyk_mykyta.ntudp.dnipro.ua/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="h" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : 'en'}" scope="session" />

<fmt:setBundle basename="text"/>

<html>
<head>
    <title>Orders for user</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
          integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
          crossorigin="anonymous">
</head>
<body>
<div class="page-wrapper">
    <div class="container">

        <div class="row">
            <c:forEach var="bean" items="${requestScope.beans}">
            <div class="order-item">
                <div class="row">
                    <div class="col s9">
                        <div class="row">
                            <div class="col s3">
                                ID:
                                <span>${bean.orderId}</span>
                            </div>
                            <div class="row">
                                <div class="col s3">
                                    <fmt:message key="label.name"/>
                                    <span>${bean.name}</span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col s3">
                                <fmt:message key="label.publisher"/>
                                <span>${bean.publisher}</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s3">
                                <fmt:message key="label.author"/>
                                <span>${bean.author}</span>
                            </div>
                        </div>
                        <c:choose>
                            <c:when test="${bean.daysCount != 0}">
                                <c:set var="delay" value="${f:delay(bean.approvedTime)}"/>
                                <div class="col s3">
                                    <c:choose>
                                        <c:when test="${delay >= bean.daysCount}">
                                            <fmt:message key="label.penaltySize"/>
                                            <span>${10 * (delay - bean.daysCount)}</span>
                                        </c:when>
                                        <c:otherwise>
                                            <fmt:message key="label.daysLeft"/>
                                            <span>${bean.daysCount - delay}</span>
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                            </c:when>
                            <c:otherwise>
                                <c:set var="delay" value="${f:delayHours(bean.approvedTime)}"/>
                                <div class="col s3">
                                    <c:choose>
                                        <c:when test="${delay > bean.hoursCount}">
                                            <fmt:message key="label.penaltySize"/>
                                            <span>${3*(delay - bean.hoursCount)}</span>
                                        </c:when>
                                        <c:otherwise>
                                            <fmt:message key="label.hoursLeft"/>
                                            <span>${bean.hoursCount - delay}</span>
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                            </c:otherwise>
                        </c:choose>
                    </div><br/>
                </div>
            </div>
        </div>
    </div>
    <br/>
    </c:forEach>
</div>
<ul class="pagination">
    <c:if test="${requestScope.currentPage != 1}">
        <li class="disabled">
            <a href="${pageContext.request.contextPath}/controller/librarian/orders-of-user?page=${requestScope.currentPage-1}&id=${requestScope.id}">                                        Previous
            </a>
        </li>
    </c:if>

    <c:forEach begin="1" end="${requestScope.numOfPages}" varStatus="i">
        <c:choose>
            <c:when test="${requestScope.currentPage eq i.index}}">
                <li class="page-active">
                    <a class="page-link">
                            ${i.index}<span class="sr-only">(current)</span>
                    </a>
                </li>
            </c:when>
            <c:otherwise>
                <li class="page-active">

                    <a class="page-link" href="${pageContext.request.contextPath}/controller/librarian/orders-of-user?page=${i.index}&id=${requestScope.id}">${i.index}</a>
                </li>
            </c:otherwise>
        </c:choose>
    </c:forEach>

    <c:if test="${requestScope.currentPage lt requestScope.numOfPages}">
        <li class="page-item">
            <input type="hidden" name="id" value="${requestScope.id}">
            <a class="page-link" href="${pageContext.request.contextPath}/controller/admin/librarian/orders-of-user?page=${requestScope.currentPage+1}&id=${requestScope.id}">Next</a>
        </li>
    </c:if>
</ul>
</div>
<h:change-language path="redirect:/FinalProject/controller/librarian/orders-of-user"/>
<%@include file="/WEB-INF/jspf/footer.jspf"%>
</body>
</html>
