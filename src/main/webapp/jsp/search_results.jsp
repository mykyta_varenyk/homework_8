<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="f" uri="http://library.varenyk_mykyta.ntudp.dnipro.ua/functions" %>
<%@ taglib prefix="h" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : 'en'}" scope="session" />

<fmt:setBundle basename="text"/>

<html>
<head>
    <title>search</title>
</head>
<body>
<table id="search-results">
    <tr>
        <td class="content">
            <c:choose>
                <c:when test="${fn:length(requestScope.bookBeans) == 0}">No such books</c:when>
                <c:otherwise>
                    <table id="search_results_table">
                        <thead>
                        <tr>
                            <td>№</td>
                            <td><fmt:message key="label.name"/></td>
                            <td><fmt:message key="label.author"/></td>
                            <td><fmt:message key="label.dateOfIssue"/></td>
                            <td><fmt:message key="label.count"/>/<fmt:message key="index_jsp.notAvailable"/></td>
                            <td><fmt:message key="index_jsp.publisher"/></td>
                        </tr>
                        </thead>
                        <c:forEach var="bean" items="${requestScope.bookBeans}">
                            <tr>
                                <td>${bean.id}</td>
                                <td>${bean.name}</td>
                                <td>
                                    <c:forEach var="authorVar" items="${bean.author}">
                                        ${authorVar}
                                    </c:forEach>
                                </td>
                                <td>${bean.dateOfIssue}</td>
                                <c:set var="count" value="${f:countOrderedBooks(requestScope.userOrderBeans,bean.id)}"/>
                                <td>
                                    <c:choose>
                                        <c:when test="${bean.count >= count}">
                                            <jsp:text>${bean.count - count}</jsp:text>
                                            <c:if test="${sessionScope.user.roleId == 2}">
                                <td>
                                    <div class="col s03">
                                        <form action="${pageContext.request.contextPath}/controller/user/take-home" method="post">
                                            <input type="number" hidden name="id" value="${bean.id}">
                                            <input type="submit" class="button" value="take book home">
                                        </form>
                                    </div>
                                    <div class="col so3">
                                        <form action="${pageContext.request.contextPath}/controller/user/in-reading-hall"
                                              method="post">
                                            <input type="number" hidden name="id" value="${bean.id}">
                                            <input type="submit" class="button" value="read book in reading hall">
                                        </form>
                                    </div>
                                </td>
                                            </c:if>
                                        </c:when>
                                        <c:otherwise>
                                            <jsp:text>not available at the moment</jsp:text>
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                                <td>${bean.publisher}</td>
                            </tr>
                        </c:forEach>
                    </table>
                </c:otherwise>
            </c:choose>
        </td>
    </tr>
</table>
<h:change-language path="search_results"/>
<%@include file="/WEB-INF/jspf/footer.jspf"%>

</body>
</html>
