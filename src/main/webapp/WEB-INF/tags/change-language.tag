<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@tag pageEncoding="UTF-8" %>

<%@attribute name="path" required="true"%>

<form method="post" action="${pageContext.request.contextPath}/controller/change-language" onchange="submit()">
    <input type="hidden" value="${path}" name="pageToReturn">
    ${requestScope.id}
    <input type="hidden" name="id" value="${requestScope.id}">
    <select name="language">
        <option value="en" ${language == 'en' ? 'selected' : ''}>English</option>
        <option value="ua" ${language == 'ua' ? 'selected' : ''}>Українська</option>
    </select>
</form>