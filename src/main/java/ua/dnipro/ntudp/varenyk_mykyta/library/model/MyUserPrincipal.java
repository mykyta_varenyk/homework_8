package ua.dnipro.ntudp.varenyk_mykyta.library.model;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class MyUserPrincipal implements UserDetails {
    private static final long serialVersionUID = -3054171651703764262L;

    private final String username;
    private final String password;
    private final List<SimpleGrantedAuthority> authorities;
    private final boolean isNonLocked;

    public MyUserPrincipal(String username, String password, List<SimpleGrantedAuthority> authorities, boolean isNonLocked) {
        this.username = username;
        this.password = password;
        this.authorities = authorities;
        this.isNonLocked = isNonLocked;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return isNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public static UserDetails fromUser(ua.dnipro.ntudp.varenyk_mykyta.library.model.User user){
        return new User(user.getLogin(),user.getPassword(),true,true,true, !user.isBlocked(), Collections.singletonList(Role.getAuthority(user)));
    }
}
