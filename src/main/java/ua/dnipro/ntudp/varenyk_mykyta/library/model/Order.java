package ua.dnipro.ntudp.varenyk_mykyta.library.model;

import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Objects;

/**
 * Order entity.
 *
 * @author Mykyta Varenyk
 */
public class Order extends Entity {
    private LocalDateTime approvedTime;
    private int accountId;
    private Long librarianId;
    private boolean isApproved;
    private boolean inReadingHall;
    private LocalDate expiredAt;
    private String expired;
    private int daysCount;
    private int hoursCount;
    private int bookId;
    private boolean isReturned;

    public Order(){}

    private Order(LocalDateTime approvedTime, int accountId, Long librarianId, boolean isApproved, boolean inReadingHall, LocalDate expiredAt, String expired, int daysCount, int hoursCount, int bookId, boolean isReturned) {
        this.approvedTime = approvedTime;
        this.accountId = accountId;
        this.librarianId = librarianId;
        this.isApproved = isApproved;
        this.inReadingHall = inReadingHall;
        this.expiredAt = expiredAt;
        this.expired = expired;
        this.daysCount = daysCount;
        this.hoursCount = hoursCount;
        this.bookId = bookId;
        this.isReturned = isReturned;
    }

    public boolean isReturned() {
        return isReturned;
    }

    public void setReturned(int isReturned) {
        this.isReturned = isReturned == 1;
    }

    public static class OrderBuilder {
        private LocalDateTime approvedTime;
        private int accountId;
        private Long librarianId;
        private boolean isApproved;
        private boolean inReadingHall;
        private LocalDate expiredAt;
        private String expired;
        private int daysCount;
        private int hoursCount;
        private int bookId;
        private boolean isReturned;

        public OrderBuilder setApprovedTime(LocalDateTime approvedTime) {
            this.approvedTime = approvedTime;
            return this;
        }

        public OrderBuilder setAccountId(int accountId) {
            this.accountId = accountId;
            return this;
        }

        public OrderBuilder setLibrarianId(Long librarianId) {
            this.librarianId = librarianId;
            return this;
        }

        public OrderBuilder setApproved(boolean approved) {
            isApproved = approved;
            return this;
        }

        public OrderBuilder setInReadingHall(boolean inReadingHall) {
            this.inReadingHall = inReadingHall;
            return this;
        }

        public OrderBuilder setExpiredAt(LocalDate expiredAt) {
            this.expiredAt = expiredAt;
            return this;
        }

        public OrderBuilder setExpired(String expired) {
            this.expired = expired;
            return this;
        }

        public OrderBuilder setDaysCount(int daysCount) {
            this.daysCount = daysCount;
            return this;
        }

        public OrderBuilder setHoursCount(int hoursCount) {
            this.hoursCount = hoursCount;
            return this;
        }

        public OrderBuilder setBookId(int bookId) {
            this.bookId = bookId;
            return this;
        }

        public OrderBuilder setReturned(int isReturned) {
            this.isReturned = isReturned == 1;
            return this;
        }

        public Order build(){
            return new Order(approvedTime,accountId,librarianId,isApproved,inReadingHall,expiredAt,expired,daysCount,hoursCount,bookId,isReturned);
        }
    }

    public static OrderBuilder builder(){
        return new OrderBuilder();
    }

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public int getHoursCount() {
        return hoursCount;
    }

    public void setHoursCount(int hoursCount) {
        this.hoursCount = hoursCount;
    }

    public String getExpired() {
        return expired;
    }

    public void setExpired(String expired) {
        this.expired = expired;
    }

    public int getDaysCount() {
        return daysCount;
    }

    public void setDaysCount(int daysCount) {
        this.daysCount = daysCount;
    }

    public LocalDateTime getApprovedTime() {
        return approvedTime;
    }

    public void setApprovedTime(Date approved_time) {
        if (approved_time != null) {
            this.approvedTime = approved_time.
                    toInstant().
                    atZone(ZoneId.systemDefault()).
                    toLocalDateTime();
        }
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountID(int account_id) {
        this.accountId = account_id;
    }

    public Long getLibrarianId() {
        return librarianId;
    }

    public void setLibrarianId(Long librarianId) {
        this.librarianId = librarianId;
    }

    public boolean getIsApproved() {
        return isApproved;
    }

    public void setApproved(int approved) {
        if (approved == 1) {
            isApproved = true;
        }
    }

    public void setApproved(boolean approved){
        isApproved = approved;
    }

    public boolean getIsInReadingHall() {
        return inReadingHall;
    }

    public void setInReadingHall(int inReadingHall) {
        if (inReadingHall == 1) {
            this.inReadingHall = true;
        }
    }

    public LocalDate getExpiredAt() {
        return expiredAt;
    }

    public void setExpiredAt(Date expiredAt) {
        if (expiredAt != null) {
            this.expiredAt = expiredAt.toInstant().
                    atZone(ZoneId.systemDefault()).
                    toLocalDate();
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return accountId == order.accountId &&
                isApproved == order.isApproved &&
                inReadingHall == order.inReadingHall &&
                daysCount == order.daysCount &&
                hoursCount == order.hoursCount &&
                bookId == order.bookId &&
                Objects.equals(approvedTime, order.approvedTime) &&
                Objects.equals(librarianId, order.librarianId) &&
                Objects.equals(expiredAt, order.expiredAt) &&
                Objects.equals(expired, order.expired);
    }

    @Override
    public int hashCode() {
        return Objects.hash(approvedTime, accountId, librarianId, isApproved, inReadingHall, expiredAt, expired, daysCount, hoursCount, bookId);
    }

    @Override
    public String toString() {
        return "Order{" +
                "approved_time=" + approvedTime +
                ", account_id=" + accountId +
                ", librarianId=" + librarianId +
                ", isApproved=" + isApproved +
                ", inReadingHall=" + inReadingHall +
                ", expiredAt=" + expiredAt +
                '}';
    }
}
