package ua.dnipro.ntudp.varenyk_mykyta.library.web.controllers;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ua.dnipro.ntudp.varenyk_mykyta.library.Path;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.bean.UserOrderBean;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.dao.OrderDao;
import ua.dnipro.ntudp.varenyk_mykyta.library.model.Author;
import ua.dnipro.ntudp.varenyk_mykyta.library.model.Book;
import ua.dnipro.ntudp.varenyk_mykyta.library.util.Util;
import ua.dnipro.ntudp.varenyk_mykyta.library.web.services.BookService;
import ua.dnipro.ntudp.varenyk_mykyta.library.web.services.ServiceUtil;
import javax.servlet.http.HttpSession;
import java.time.LocalDate;
import java.util.*;

@Controller
public class BookController {
    private static final Logger LOGGER = LogManager.getLogger(BookController.class);

    private final BookService bookService;

    private final HttpSession session;

    private final OrderDao orderDao;

    @Autowired
    public BookController(BookService bookService, HttpSession session, OrderDao orderDao) {
        this.bookService = bookService;
        this.session = session;
        this.orderDao = orderDao;
    }

    @GetMapping(value = "*/search")
    @ResponseBody
    public ModelAndView findBooks(@RequestParam(value = "search-by-author",required = false) String searchByAuthor,
                                  @RequestParam(value = "author-id",required = false) String authorId,
                                  @RequestParam(value = "search-by-name",required = false) String searchByName,
                                  @RequestParam(value = "name-id",required = false)  String nameId){
        ModelAndView modelAndView;

        String languageCode = Util.getLanguageCodeFromSession(session);

        List<Book> books = bookService.findBooks(languageCode,searchByAuthor,searchByName,authorId,nameId);

        if (books == null){
            String errorMessage = "No arguments have been selected";

            LOGGER.error("ERROR: {}",errorMessage);

            modelAndView = new ModelAndView(Path.PAGE_ERROR_PAGE);

            modelAndView.addObject("errorMessage",errorMessage);

            return modelAndView;
        }

        List<UserOrderBean> userOrderBeans = orderDao.getAllUserOrderBeans(languageCode);

        LOGGER.debug("get orders -> {}",userOrderBeans);

        modelAndView = new ModelAndView(Path.PAGE_SEARCH);

        modelAndView.addObject("bookBeans",books);

        modelAndView.addObject("userOrderBeans",userOrderBeans);

        return modelAndView;
    }

    @GetMapping(value = "*/sort")
    @ResponseBody
    public ModelAndView sortBooks(@RequestParam("sort_by") String sortBy){
        ModelAndView modelAndView;

        LOGGER.debug("selected sorting parameter -> {}",sortBy);

        List<Book> booksList = (List<Book>) session.getAttribute("booksList");

        LOGGER.debug("presorted books -> {}",booksList);

        booksList = bookService.sortBooks(booksList,sortBy);

        LOGGER.debug("sorted catalog -> {}",booksList);

        session.setAttribute("booksList",booksList);

        modelAndView = new ModelAndView(Path.PAGE_INDEX_REDIRECT);

        modelAndView.addObject("booksList",booksList);

        return modelAndView;
    }

    @PostMapping(value = "*/change-language")
    @ResponseBody
    public ModelAndView changeLanguage(@RequestParam(value = "id") int id,
                                       @RequestParam("pageToReturn") String pageToReturn){
        bookService.changeLanguage(session,id);

        return new ModelAndView(pageToReturn);
    }

    @GetMapping(value = "*/admin/books")
    @ResponseBody
    public ModelAndView getAdminBooksPage(@RequestParam(value = "dateOfIssueInTheFuture",required = false) String dateOfIssueInTheFuture,
                                          @RequestParam(value = "page",required = false) String page){
        ModelAndView modelAndView;

        modelAndView = new ModelAndView(Path.PAGE_ADMIN_BOOKS_PAGE);

        String languageCode = Util.getLanguageCodeFromSession(session);

        if (dateOfIssueInTheFuture != null){
            LOGGER.debug(dateOfIssueInTheFuture);

            modelAndView.addObject("dateOfIssueInTheFuture",dateOfIssueInTheFuture);
        }

        int currentPage = 1;

        int recordsPerPage = 2;

        if (page != null){
            currentPage = Integer.parseInt(page);
        }

        LOGGER.debug("currentPage -> {}",currentPage);

        List<Book> books = bookService.getBooksWithPagination(currentPage,recordsPerPage,languageCode);

        modelAndView.addObject("books",books);

        List<Author> authors = bookService.getAllAuthors(languageCode);

        LOGGER.debug("authors -> {}",authors);

        List<String> languages = Arrays.asList("en","ua");

        //List<String> languages = DBManager.getLanguages();

        LOGGER.debug("languages -> {}",languages);

        modelAndView.addObject("currentPage",currentPage);

        modelAndView.addObject("recordsPerPage",recordsPerPage);

        modelAndView.addObject("numOfPages",ServiceUtil.getNumberOfPages(bookService.getAmountOfBooks(),recordsPerPage));

        modelAndView.addObject("authors",authors);

        modelAndView.addObject("languages",languages);

        return modelAndView;
    }

    @PostMapping(value = "*/admin/create-book")
    public ModelAndView createBook(@RequestParam("authorId") String authorId,
                                   @RequestParam(value = "dateOfIssueInTheFuture",required = false) String dateOfIssueInTheFuture,
                                   @ModelAttribute("book") Book book){
        String languageCode = Util.getLanguageCodeFromSession(session);

        String errorMessage;

        LOGGER.debug("author id -> {}",authorId);

        if (book.getDateOfIssue() != null && validateDate(book.getDateOfIssue())){
            errorMessage = ResourceBundle.getBundle("text", Locale.forLanguageTag(languageCode))
                    .getString("label.dateOfIssueInTheFuture");

            session.setAttribute("dateOfIssueInTheFuture",errorMessage);

            LOGGER.error("errorMessage -> {}",errorMessage);

            return new ModelAndView(Path.PAGE_ADMIN_BOOKS_PAGE_REDIRECT);
        }

        bookService.createBook(book,Integer.parseInt(authorId),languageCode);

        return new ModelAndView(Path.PAGE_ADMIN_BOOKS_PAGE_REDIRECT);
    }

    private boolean validateDate(LocalDate date) {
        LOGGER.debug("user given date -> {}",date);

        return !date.isBefore(LocalDate.now());
    }
}
