package ua.dnipro.ntudp.varenyk_mykyta.library.web.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.dnipro.ntudp.varenyk_mykyta.library.Path;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.DBManager;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.dao.Fields;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.dao.UserDao;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Delete a librarian.
 *
 * @author Mykyta Varenyk
 *
 */
@Service
public class DeleteLibrarianCommand extends Command{
    private static Logger LOGGER = LogManager.getLogger(DeleteLibrarianCommand.class);

    private UserDao userDao;

    @Autowired
    public DeleteLibrarianCommand(UserDao userDao){
        this.userDao = userDao;
    }

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        LOGGER.debug("DeleteLibrarianCommand started");

        int id = Integer.parseInt(req.getParameter(Fields.ENTITY_ID));

        LOGGER.debug("librarian id -> {}",id);

        try {
            userDao = new UserDao(DBManager.getDataSource());
        } catch (NamingException e) {
            e.printStackTrace();
        }

        boolean result = userDao.deleteLibrarian(id);

        LOGGER.debug("librarian deleted -> {}",result);

        LOGGER.debug("DeleteLibrarianCommand finished");

        return Path.PAGE_ADMIN_USERS_PAGE_REDIRECT;
    }
}
