package ua.dnipro.ntudp.varenyk_mykyta.library.web.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.dnipro.ntudp.varenyk_mykyta.library.Path;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.DBManager;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.dao.BookDao;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.dao.Fields;
import ua.dnipro.ntudp.varenyk_mykyta.library.model.Book;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.time.LocalDate;
import java.util.Collections;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Create a book.
 *
 * @author Mykyta Varenyk
 *
 */
@Service
public class CreateBookCommand extends Command {
    private static Logger LOGGER = LogManager.getLogger(CreateBookCommand.class);

    private BookDao bookDao;
    @Autowired
    public CreateBookCommand(BookDao bookDao){
        this.bookDao = bookDao;
    }

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        LOGGER.debug("CreateBookCommand started");
        HttpSession session = req.getSession();

        String languageCode,errorMessage;

        if (session != null){
            languageCode = (String) session.getAttribute("language");
        }else {
            languageCode = "en";
        }

        LOGGER.debug("language code -> {}",languageCode);

        int authorId = Integer.parseInt(req.getParameter("authorId"));

        LOGGER.debug("author id -> {}",authorId);

        if (validateDate(req.getParameter(Fields.BOOK_DATE_OF_ISSUE))){
            errorMessage = ResourceBundle.getBundle("text", Locale.forLanguageTag(languageCode))
                    .getString("label.dateOfIssueInTheFuture");

            session.setAttribute("dateOfIssueInTheFuture",errorMessage);

            LOGGER.error("errorMessage -> {}",errorMessage);

            return Path.PAGE_ADMIN_BOOKS_PAGE_REDIRECT;
        }

        Book book = getBook(req, bookDao, authorId, languageCode);

        LOGGER.debug("book to insert -> {}", book);

        boolean result = bookDao.createBook(book, authorId);

        LOGGER.debug("book inserted -> {}", result);

        LOGGER.debug("CreateBookCommand finished");

        return Path.PAGE_ADMIN_BOOKS_PAGE_REDIRECT;
    }

    private boolean validateDate(String dateString) {
        LocalDate date1 = LocalDate.parse(dateString);

        LOGGER.debug("user given date -> {}",date1);

        return !date1.isBefore(LocalDate.now());
    }

    private Book getBook(HttpServletRequest request,BookDao bookDao,int id,String languageCode){
        Book book = new Book();

        book.setAuthor(Collections.singletonList(bookDao.getAuthorById(id,languageCode)));
        book.setDescription(request.getParameter(Fields.BOOK_DESCRIPTION + " English"));
        book.setDescriptionUa(request.getParameter(Fields.BOOK_DESCRIPTION + " Ukrainian"));
        book.setDate(request.getParameter(Fields.BOOK_DATE_OF_ISSUE));
        book.setCount(Integer.parseInt(request.getParameter(Fields.BOOK_COUNT)));
        book.setPublisher(request.getParameter(Fields.BOOK_PUBLISHER + " English"));
        book.setPublisherUa(request.getParameter(Fields.BOOK_PUBLISHER + " Ukrainian"));
        book.setName(request.getParameter(Fields.ENTITY_NAME + " English"));
        book.setNameUa(request.getParameter(Fields.ENTITY_NAME + " Ukrainian"));
        return book;
    }
}
