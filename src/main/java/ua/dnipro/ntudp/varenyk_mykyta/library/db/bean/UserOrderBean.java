package ua.dnipro.ntudp.varenyk_mykyta.library.db.bean;

import ua.dnipro.ntudp.varenyk_mykyta.library.model.Entity;

import javax.jws.soap.SOAPBinding;
import java.time.LocalDateTime;

/**
 * Provide records for virtual table:
 * <pre>
 * |order.id|book_translation.name|book_translation.publisher|author.name|order.given_by_librarian|order.days_count|order.hours_count|order.approved_time
 * </pre>
 *
 * @author Mykyta Varenyk
 */

public class UserOrderBean extends Entity {
    private static final long serialVersionUID = 1521504941650778061L;

    private int userId;
    private long orderId;
    private int bookId;
    private String publisher;
    private String author;
    private String name;
    private int daysCount;
    private int hoursCount;
    private int librarianId;
    private LocalDateTime approvedTime;
    private boolean inReadingHall;
    private boolean isReturned;

    public UserOrderBean() {
    }

    private UserOrderBean(int userId, long orderId, int bookId, String publisher, String author, String name, int daysCount, int hoursCount, int librarianId, LocalDateTime approvedTime, boolean inReadingHall, boolean isReturned) {
        this.userId = userId;
        this.orderId = orderId;
        this.bookId = bookId;
        this.publisher = publisher;
        this.author = author;
        this.name = name;
        this.daysCount = daysCount;
        this.hoursCount = hoursCount;
        this.librarianId = librarianId;
        this.approvedTime = approvedTime;
        this.inReadingHall = inReadingHall;
        this.isReturned = isReturned;
    }

    public static UserOrderBeanBuilder builder(){
        return new UserOrderBeanBuilder();
    }

    public static class UserOrderBeanBuilder {
        private int userId;
        private long orderId;
        private int bookId;
        private String publisher;
        private String author;
        private String name;
        private int daysCount;
        private int hoursCount;
        private int librarianId;
        private LocalDateTime approvedTime;
        private boolean inReadingHall;
        private boolean isReturned;

        public UserOrderBeanBuilder setUserId(int userId) {
            this.userId = userId;
            return this;
        }

        public UserOrderBeanBuilder setOrderId(long orderId) {
            this.orderId = orderId;
            return this;
        }

        public UserOrderBeanBuilder setBookId(int bookId) {
            this.bookId = bookId;
            return this;
        }

        public UserOrderBeanBuilder setPublisher(String publisher) {
            this.publisher = publisher;
            return this;
        }

        public UserOrderBeanBuilder setAuthor(String author) {
            this.author = author;
            return this;
        }

        public UserOrderBeanBuilder setName(String name) {
            this.name = name;
            return this;
        }

        public UserOrderBeanBuilder setDaysCount(int daysCount) {
            this.daysCount = daysCount;
            return this;
        }

        public UserOrderBeanBuilder setHoursCount(int hoursCount) {
            this.hoursCount = hoursCount;
            return this;
        }

        public UserOrderBeanBuilder setLibrarianId(int librarianId) {
            this.librarianId = librarianId;
            return this;
        }

        public UserOrderBeanBuilder setApprovedTime(LocalDateTime approvedTime) {
            this.approvedTime = approvedTime;
            return this;
        }

        public UserOrderBeanBuilder setInReadingHall(boolean inReadingHall) {
            this.inReadingHall = inReadingHall;
            return this;
        }

        public UserOrderBeanBuilder setReturned(boolean returned) {
            isReturned = returned;
            return this;
        }

        public UserOrderBean build() {
            return new UserOrderBean(userId, orderId, bookId, publisher, author, name, daysCount, hoursCount, librarianId, approvedTime, inReadingHall, isReturned);
        }

    }

    public boolean isReturned() {
        return isReturned;
    }

    public void setReturned(int isReturned) {
        this.isReturned = isReturned == 1;
    }

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public int getHoursCount() {
        return hoursCount;
    }

    public void setHoursCount(int hoursCount) {
        this.hoursCount = hoursCount;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public boolean isInReadingHall() {
        return inReadingHall;
    }

    public void setInReadingHall(int inReadingHall) {
        if (inReadingHall == 1)
            this.inReadingHall = true;
        else
            this.inReadingHall = false;
    }

    public LocalDateTime getApprovedTime() {
        return approvedTime;
    }

    public void setApprovedTime(LocalDateTime approvedTime) {
        this.approvedTime = approvedTime;
    }

    public void setApprovedTime(String approvedTime) {
        this.approvedTime = LocalDateTime.parse(approvedTime);
    }

    public int getDaysCount() {
        return daysCount;
    }

    public void setDaysCount(int daysCount) {
        this.daysCount = daysCount;
    }

    public long getOrderId() {
        return orderId;
    }

    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLibrarianId() {
        return librarianId;
    }

    public void setLibrarianId(int librarianId) {
        this.librarianId = librarianId;
    }

    @Override
    public String toString() {
        return "UserOrderBean{" +
                "orderId=" + orderId +
                ", publisher='" + publisher + '\'' +
                ", author='" + author + '\'' +
                ", name='" + name + '\'' +
                ", librarianId=" + librarianId +
                '}';
    }
}
