package ua.dnipro.ntudp.varenyk_mykyta.library.web.controllers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ua.dnipro.ntudp.varenyk_mykyta.library.Path;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.dao.Fields;
import ua.dnipro.ntudp.varenyk_mykyta.library.model.User;
import ua.dnipro.ntudp.varenyk_mykyta.library.util.Util;
import ua.dnipro.ntudp.varenyk_mykyta.library.web.services.ServiceUtil;
import ua.dnipro.ntudp.varenyk_mykyta.library.web.services.UserService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class UserController {
    private static final Logger LOGGER = LogManager.getLogger(UserController.class);

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping(value = "*/validate-login")
    public ModelAndView login(
            HttpServletRequest req
    ){
        LOGGER.debug("UsersController#login started");

        ModelAndView modelAndView;

       HttpSession session = req.getSession();

        String login = req.getParameter(Fields.USER_LOGIN);

        String password = req.getParameter(Fields.USER_PASSWORD);

        String errorMessage;

        String languageCode = Util.getLanguageCodeFromSession(session);

        modelAndView = new ModelAndView(Path.PAGE_ERROR_PAGE);

        if (login == null || password == null || login.isEmpty() || password.isEmpty()) {
           errorMessage = "Login/password cannot be empty";
           LOGGER.error("errorMessage -> {}",errorMessage);
           modelAndView.addObject("errorMessage",errorMessage);
           return modelAndView;
        }
       return userService.validateLoginInformation(login,password,languageCode,session);
    }

    @GetMapping("*/login")
    public String getLoginPage(){
        return "login";
    }

    @GetMapping(value = "*/login?error")
    public ModelAndView loginError(){
        return new ModelAndView(Path.PAGE_LOGIN);
   }

    @GetMapping(value = "/user/home")
    public ModelAndView userHomePage(){
        return new ModelAndView(Path.PAGE_USER_HOME_PAGE);
    }

    @GetMapping(value = "/admin/home")
    public ModelAndView adminHomePage(){
       return new ModelAndView(Path.PAGE_ADMIN_HOME_PAGE);
   }

    @GetMapping(value = "/librarian/home")
    public ModelAndView librarianHomePage(){
       return new ModelAndView(Path.PAGE_LIBRARIAN_HOME_PAGE);
   }

   @PostMapping(value = "*/logout")
   public ModelAndView logout(HttpServletRequest req){
        HttpSession session = req.getSession(false);

        if (session != null) {
            session.invalidate();
            LOGGER.debug("Session invalidated");
        }else{
            LOGGER.debug("there is no active session");
        }

        return new ModelAndView(Path.PAGE_INDEX_REDIRECT);
  }

    @GetMapping(value = "*/admin/users")
    public ModelAndView adminUsersPage(@RequestParam(value = "page",required = false) String page){
       ModelAndView modelAndView = new ModelAndView(Path.PAGE_ADMIN_USERS_PAGE);

        int currentPage = 1;

        int recordsPerPage = 2;

        if (page != null){
            currentPage = Integer.parseInt(page);
        }

        LOGGER.debug("page -> {}",currentPage);

        List<User> users = userService.getUsersWithPagination(currentPage,recordsPerPage);

        LOGGER.debug("users with limit:{} and offset:{} -> {}",currentPage,recordsPerPage,users);

        modelAndView.addObject("users",users);

        modelAndView.addObject("currentPage",currentPage);

        modelAndView.addObject("recordsPerPage",recordsPerPage);

        modelAndView.addObject("numOfPages", ServiceUtil.getNumberOfPages(userService.countUsers(),recordsPerPage));

        return modelAndView;
    }

    @PostMapping(value = "*/register")
    public ModelAndView registerUser(HttpServletRequest req){
        return new ModelAndView(userService.registerUser(req));
    }
}
