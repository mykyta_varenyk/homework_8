package ua.dnipro.ntudp.varenyk_mykyta.library.web.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.DBManager;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.dao.BookDao;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.dao.OrderDao;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.dao.UserDao;

import javax.naming.NamingException;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.TreeMap;

/**
 * Holder for all commands.<br/>
 *
 * @author Mykyta Varenyk
 *
 */

public class CommandContainer {
    private static Map<String, Command> commands = new TreeMap<>();

    private static Logger LOGGER = LogManager.getLogger(CommandContainer.class);

    static {
        commands.put("login",new LoginCommand());
        commands.put("admin/home",new GetAdminHomePageCommand());
        commands.put("user/home",new GetUserHomePageCommand());
        commands.put("user/take-home",new GetMakeOrderTakeHomePageCommand());
        commands.put("user/in-reading-hall",new GetMakeOrderInReadingHallPageCommand());
        commands.put("librarian/home",new GetLibrarianHomePageCommand());
        commands.put("logout",new LogoutCommand());
        commands.put("",new GetMainPageCommand());
        try {
            commands.put("admin/block-user",new BlockUserCommand(new UserDao(DBManager.getDataSource())));
            commands.put("admin/create-book",new CreateBookCommand(new BookDao(DBManager.getDataSource())));
            commands.put("admin/create-librarian",new CreateLibrarianCommand(new UserDao(DBManager.getDataSource())));
            commands.put("admin/delete-book",new DeleteBookCommand(new BookDao(DBManager.getDataSource())));
            commands.put("admin/delete-librarian",new DeleteLibrarianCommand(new UserDao(DBManager.getDataSource())));
            commands.put("admin/update-book",new EditBookCommand(new BookDao(DBManager.getDataSource())));
            commands.put("admin/books",new GetBooksPageCommand(new BookDao(DBManager.getDataSource())));
            commands.put("admin/edit-book",new GetEditBookPageCommand(new BookDao(DBManager.getDataSource())));
            commands.put("admin/users",new GetUsersPageCommand(new UserDao(DBManager.getDataSource())));
            commands.put("admin/unblock-user",new UnblockUserCommand(new UserDao(DBManager.getDataSource())));

            commands.put("librarian/approve-order",new ApproveOrderCommand(new OrderDao(DBManager.getDataSource())));
            commands.put("librarian/orders-of-user",new GetOrdersForUserPageCommand(new OrderDao(DBManager.getDataSource())));
            commands.put("librarian/disapprove-order",new DisapproveOrderCommand(new OrderDao(DBManager.getDataSource())));
            commands.put("librarian/orders",new GetListUserOrdersPageCommand(new OrderDao(DBManager.getDataSource())));
            commands.put("librarian/users",new GetListUsersForLibrarianPageCommand(new UserDao(DBManager.getDataSource())));

            commands.put("user/return-book",new ReturnBookCommand(new OrderDao(DBManager.getDataSource())));
            commands.put("user/orders",new GetOrderedBooksPageCommand(new OrderDao(DBManager.getDataSource())));
            commands.put("user/confirm-in-reading-hall-order",new MakeInReadingHallOrderCommand(new OrderDao(DBManager.getDataSource())));
            commands.put("user/confirm-take-home-order",new MakeTakeHomeOrderCommand(new OrderDao(DBManager.getDataSource())));

            commands.put("change-language",new ChangeLanguageCommand(new BookDao(DBManager.getDataSource())));
            commands.put("register", new RegisterCommand(new UserDao(DBManager.getDataSource())));
            commands.put("search",new SearchThroughCatalogCommand(new BookDao(DBManager.getDataSource()),new OrderDao(DBManager.getDataSource())));
        } catch (NamingException e) {
            LOGGER.error("NamingException in CommandContainer in static block -> {}",e);
        }
        commands.put("sort",new SortCatalogCommand());
    }

    /**
     * Returns command object with the given name.
     *
     * @param commandName name of the command.
     * @return Command object.
     */
    public static Command getCommand(String commandName){
        if (commandName == null || !commandName.contains(commandName)){
            LOGGER.error("command {} not found",commandName);
            throw new NoSuchElementException();
        }

        return commands.get(commandName);
    }
}
