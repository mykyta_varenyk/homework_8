package ua.dnipro.ntudp.varenyk_mykyta.library.model;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Objects;

/**
 * Book entity.
 *
 * @author Mykyta Varenyk
 */
public class Book extends Entity {
    private static final long serialVersionUID = -3828575691234792388L;
    private String name;
    private List<String> author;
    private LocalDate dateOfIssue;
    private String date;
    private String publisher;
    private String description;
    private int count;
    private String publisherUa;
    private String descriptionUa;
    private String nameUa;
    private boolean isReturned;

    public Book(){}

    private Book(int id,String name, List<String> author, LocalDate dateOfIssue, String date, String publisher, String description, int count, String publisherUa, String descriptionUa, String nameUa, boolean isReturned) {
        this.name = name;
        this.author = author;
        this.dateOfIssue = dateOfIssue;
        this.date = date;
        this.publisher = publisher;
        this.description = description;
        this.count = count;
        this.publisherUa = publisherUa;
        this.descriptionUa = descriptionUa;
        this.nameUa = nameUa;
        this.isReturned = isReturned;
        this.setId(id);
    }

    private final  DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    public LocalDate getDateOfIssue() {
        return dateOfIssue;
    }

    public void setDateOfIssue(String dateOfIssue) {
        this.dateOfIssue = LocalDate.parse(dateOfIssue,dateTimeFormatter);
    }

    public boolean isReturned() {
        return isReturned;
    }

    public void setReturned(boolean returned) {
        isReturned = returned;
    }

    public String getPublisherUa() {
        return publisherUa;
    }

    public void setPublisherUa(String publisherUa) {
        this.publisherUa = publisherUa;
    }

    public String getDescriptionUa() {
        return descriptionUa;
    }

    public void setDescriptionUa(String descriptionUa) {
        this.descriptionUa = descriptionUa;
    }

    public String getNameUa() {
        return nameUa;
    }

    public void setNameUa(String nameUa) {
        this.nameUa = nameUa;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getAuthor() {
        return author;
    }

    public void setAuthor(List<String> author) {
        this.author = author;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public static BookBuilder builder(){
        return new BookBuilder();
    }

    @Override
    public boolean equals(Object o){
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Book book = (Book) o;

        return this.getId() == book.getId();
    }

    public static class BookBuilder{
        private String name;
        private List<String> author;
        private LocalDate dateOfIssue;
        private String date;
        private String publisher;
        private String description;
        private int count;
        private String publisherUa;
        private String descriptionUa;
        private String nameUa;
        private boolean isReturned;
        private int id;

        public BookBuilder setId(int id) {
            this.id = id;
            return this;
        }

        public BookBuilder setName(String name) {
            this.name = name;
            return this;
        }

        public BookBuilder setAuthor(List<String> author) {
            this.author = author;
            return this;
        }

        public BookBuilder setDateOfIssue(LocalDate dateOfIssue) {
            this.dateOfIssue = dateOfIssue;
            return this;
        }

        public BookBuilder setDate(String date) {
            this.date = date;
            return this;
        }

        public BookBuilder setPublisher(String publisher) {
            this.publisher = publisher;
            return this;
        }

        public BookBuilder setDescription(String description) {
            this.description = description;
            return this;
        }

        public BookBuilder setCount(int count) {
            this.count = count;
            return this;
        }

        public BookBuilder setPublisherUa(String publisherUa) {
            this.publisherUa = publisherUa;
            return this;
        }

        public BookBuilder setDescriptionUa(String descriptionUa) {
            this.descriptionUa = descriptionUa;
            return this;
        }

        public BookBuilder setNameUa(String nameUa) {
            this.nameUa = nameUa;
            return this;
        }

        public BookBuilder setReturned(boolean returned) {
            isReturned = returned;
            return this;
        }

        public Book build(){
            return new Book(id,name,
                    author,dateOfIssue,date,publisher,description,count,publisherUa,descriptionUa,nameUa,isReturned);
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }

    @Override
    public String toString() {
        return "Book{" +
                "name='" + name + '\'' +
                ", author=" + author +
                ", dateOfIssue=" + dateOfIssue +
                ", date='" + date + '\'' +
                ", publisher='" + publisher + '\'' +
                ", description='" + description + '\'' +
                ", count=" + count +
                ", publisherUa='" + publisherUa + '\'' +
                ", descriptionUa='" + descriptionUa + '\'' +
                ", nameUa='" + nameUa + " }";
    }
}
