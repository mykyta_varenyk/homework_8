package ua.dnipro.ntudp.varenyk_mykyta.library.web.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.dnipro.ntudp.varenyk_mykyta.library.Path;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.DBManager;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.bean.UserOrderBean;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.dao.BookDao;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.dao.OrderDao;
import ua.dnipro.ntudp.varenyk_mykyta.library.model.Book;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.List;

/**
 * Search through catalog.
 *
 * @author Mykyta Varenyk
 *
 */
@Service
public class SearchThroughCatalogCommand extends Command {

    private static Logger LOGGER = LogManager.getLogger(SearchThroughCatalogCommand.class);

    private BookDao bookDao;

    private OrderDao orderDao;

    @Autowired
    public SearchThroughCatalogCommand(BookDao bookDao,OrderDao orderDao){
        this.bookDao = bookDao;
        this.orderDao=orderDao;
    }

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        LOGGER.debug("Search command started");

        HttpSession session = req.getSession();

        String languageCode;

        if (session != null){
            languageCode = (String) session.getAttribute("language");
        } else {
            languageCode = "en";
        }

        LOGGER.debug("languageCode -> {}",languageCode);

        String searchByAuthor = req.getParameter("search_by_author");

        String searchByName = req.getParameter("search_by_name");

        LOGGER.debug("searchByAuthor -> {} and searchByName -> {}",searchByAuthor,searchByName);

        List<Book> bookBeans;

        if (searchByAuthor != null && searchByAuthor.equals("yes") && searchByName != null && searchByName.equals("yes")){
            bookBeans = bookDao.
                    findBookBeansByNameAndAuthor(languageCode,
                            req.getParameter("authorId"),
                            req.getParameter("nameId"));
        }else if(searchByAuthor != null && searchByName == null){
            bookBeans = bookDao.
                    findBookBeansByAuthors(languageCode,req.getParameter("authorId"));
        }else if(searchByAuthor == null && searchByName!= null){
            bookBeans = bookDao.
                    findBookBeansByName(languageCode,req.getParameter("nameId"));
        }else {
            String errorMessage = "No arguments have been selected";
            LOGGER.error("ERROR: {}",errorMessage);
            req.setAttribute("errorMessage",errorMessage);
            return Path.PAGE_ERROR_PAGE;
        }

        LOGGER.debug("books found -> {}",bookBeans);

        List<UserOrderBean> userOrderBeans = orderDao.getAllUserOrderBeans(languageCode);

        LOGGER.debug("get orders -> {}",userOrderBeans);

        req.setAttribute("bookBeans",bookBeans);

        req.setAttribute("userOrderBeans",userOrderBeans);

        LOGGER.debug("Search command finished");
        return Path.PAGE_SEARCH;
    }
}

