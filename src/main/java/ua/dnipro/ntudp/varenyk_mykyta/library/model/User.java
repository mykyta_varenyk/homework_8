package ua.dnipro.ntudp.varenyk_mykyta.library.model;

/**
 * User entity.
 *
 * @author Mykyta Varenyk
 *
 */

public class User extends Entity {
    private static final long serialVersionUID = 5070577245705881836L;

    private String login;

    private String email;

    private String password;

    private int roleId;

    private String firstName;

    private String lastName;

    private String firstNameUa;

    private String lastNameUa;

    private String role;

    private boolean isBlocked;

    private boolean hasOrders;

    public User(){}

    private User(String login, String email, String password, int roleId, String firstName, String lastName, String firstNameUa, String lastNameUa, String role, boolean isBlocked, boolean hasOrders) {
        this.login = login;
        this.email = email;
        this.password = password;
        this.roleId = roleId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.firstNameUa = firstNameUa;
        this.lastNameUa = lastNameUa;
        this.role = role;
        this.isBlocked = isBlocked;
        this.hasOrders = hasOrders;
    }

    public static UserBuilder builder(){
        return new UserBuilder();
    }

    public static class UserBuilder{
        private String login;

        private String email;

        private String password;

        private int roleId;

        private String firstName;

        private String lastName;

        private String firstNameUa;

        private String lastNameUa;

        private String role;

        private boolean isBlocked;

        private boolean hasOrders;



        public UserBuilder setLogin(String login) {
            this.login = login;
            return this;
        }

        public UserBuilder setEmail(String email) {
            this.email = email;
            return this;
        }

        public UserBuilder setPassword(String password) {
            this.password = password;
            return this;
        }

        public UserBuilder setRoleId(int roleId) {
            this.roleId = roleId;
            return this;
        }

        public UserBuilder setFirstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public UserBuilder setLastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public UserBuilder setFirstNameUa(String firstNameUa) {
            this.firstNameUa = firstNameUa;
            return this;
        }

        public UserBuilder setLastNameUa(String lastNameUa) {
            this.lastNameUa = lastNameUa;
            return this;
        }

        public UserBuilder setRole(String role) {
            this.role = role;
            return this;
        }

        public UserBuilder setBlocked(boolean blocked) {
            isBlocked = blocked;
            return this;
        }

        public UserBuilder setHasOrders(boolean hasOrders) {
            this.hasOrders = hasOrders;
            return this;
        }

        public User build(){
            return new User(login,email,password,roleId,firstName,lastName,firstNameUa,lastNameUa,role,isBlocked,hasOrders);
        }
    }

    public String getFirstNameUa() {
        return firstNameUa;
    }

    public void setFirstNameUa(String firstNameUa) {
        this.firstNameUa = firstNameUa;
    }

    public String getLastNameUa() {
        return lastNameUa;
    }

    public void setLastNameUa(String lastNameUa) {
        this.lastNameUa = lastNameUa;
    }

    public void setBlocked(boolean blocked) {
        isBlocked = blocked;
    }

    public boolean getHasOrders() {
        return hasOrders;
    }

    public void setHasOrders(int hasOrders) {
        this.hasOrders = hasOrders == 1;
    }

    public boolean isBlocked() {
        return isBlocked;
    }

    public void setBlocked(int blocked) {
        isBlocked = blocked == 1;

    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return "User{" +
                "login='" + login + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", roleId=" + roleId +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", firstNameUa='" + firstNameUa + '\'' +
                ", lastNameUa='" + lastNameUa + '\'' +
                ", role='" + role + '\'' +
                ", isBlocked=" + isBlocked +
                ", hasOrders=" + hasOrders +
                '}';
    }
}
