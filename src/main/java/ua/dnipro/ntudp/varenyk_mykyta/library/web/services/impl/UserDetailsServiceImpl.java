package ua.dnipro.ntudp.varenyk_mykyta.library.web.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.dao.UserDao;
import ua.dnipro.ntudp.varenyk_mykyta.library.model.MyUserPrincipal;
import ua.dnipro.ntudp.varenyk_mykyta.library.model.Role;
import ua.dnipro.ntudp.varenyk_mykyta.library.model.User;

import java.util.Collections;

public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    private UserDao userDao;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User user = userDao.findUserByLogin(s);

        return new MyUserPrincipal(user.getLogin(),
                user.getPassword(),
                Collections.singletonList(Role.getAuthority(user)),
                !user.isBlocked());
    }
}
