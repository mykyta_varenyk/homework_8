package ua.dnipro.ntudp.varenyk_mykyta.library;

import com.sun.org.apache.bcel.internal.generic.IF_ACMPEQ;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.bean.UserOrderBean;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Locale;

import static java.time.temporal.ChronoUnit.DAYS;
import static java.time.temporal.ChronoUnit.HOURS;

/**
 * Class for functions.tld(tag library)
 *
 * @author Mykyta Varenyk
 */
public final class Functions {
    private Functions() {}
    public static long getDelay(LocalDateTime approvedTime){
         return DAYS.between(approvedTime,LocalDateTime.now());
    }

    public static String concatFieldWithLanguage(String field, String language){
        if(!"en".equals(language)) {
            char oldChar = language.charAt(1);
            language = language.toUpperCase();
            language = language.replace(language.charAt(1), oldChar);
            return field + language;
        }

        return field;
    }

    public static long getDelayHours(LocalDateTime approvedTime){
        return HOURS.between(approvedTime,LocalDateTime.now());
    }

    public static int countOrderedBooks(List<UserOrderBean> userOrderBeans,int id){
        int booksOrdered=0;
        for (UserOrderBean bean : userOrderBeans) {
            if (bean.getBookId() == id && !bean.isReturned()){
                booksOrdered++;
            }
        }
        return booksOrdered;
    }
}
